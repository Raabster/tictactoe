package com.raabster;

import javafx.util.Pair;

import java.util.Scanner;


public class Game {

    public boolean running = true;
    private final int SIZE = 3;
    private String[][] board = new String[SIZE][SIZE];
    private int playedRow = 0;
    private int playedColumn = 0;
    private String currentPlayer = "X";
    private int turns = 0;
    Pair[] positionTranslator =
            new Pair[]{
                    new Pair<>(0, 0), new Pair<>(0, 1), new Pair<>(0, 2),
                    new Pair<>(1, 0), new Pair<>(1, 1), new Pair<>(1, 2),
                    new Pair<>(2, 0), new Pair<>(2, 1), new Pair<>(2, 2)
            };

    /**
     *  Constructor function.
     *  Set up of the board.
     */
    public Game()
    {
        initBoard();
    }

    /**
     * Initialize the board array with empty spaces
     */
    private void initBoard()
    {
        for(int row = 0; row < SIZE; row++)
        {
            for(int col = 0; col < SIZE; col++)
            {
                board[row][col] = " ";
            }
        }
    }

    /**
     * Prints the Tic Tac Toe board as a 3x3 grid with "|" between all numbers to the console.
     */
    public void showBoard()
    {
        for(int row = 0; row < SIZE; row ++)
        {
            System.out.println();
            System.out.print("|");
            for(int col = 0; col < SIZE; col++)
            {
                System.out.print(board[row][col] + "|");
            }
        }
        System.out.println();
        System.out.println("-------");// Print to separate from next call to showBoard.
    }

    /**
     * Get input from player regarding where to place X or O and place the symbol on board.
     */
    public void playerAction()
    {
        String playerInput = "";
        Scanner scanner = new Scanner(System.in);

        while (true)
        {
            System.out.print("\nEnter position: ");
            playerInput = scanner.nextLine();

            String regex = "[1-9]";
            if (!playerInput.matches(regex))
            {
                System.out.println("Invalid input. Enter number between 0-8");
                continue;
            }

            playedRow = (int) positionTranslator[Integer.parseInt(playerInput) - 1].getKey();
            playedColumn = (int) positionTranslator[Integer.parseInt(playerInput) - 1].getValue();

            if (!isCellEmpty()) {
                System.out.println("Can only play on empty cells, try again!");
                continue;
            }

            break;
        }

        board[playedRow][playedColumn] = currentPlayer;
        turns++;
        checkState();
        nextPlayer();
    }

    /**
     * Sets next player
     */
    private void nextPlayer() {
        if (this.currentPlayer == "X") {
            this.currentPlayer = "O";
        } else {
            this.currentPlayer = "X";
        }
    }

    /**
     * Check if cell is already populated
     * @return true if the cell is empty, false otherwise
     */
    private boolean isCellEmpty() {
        return board[playedRow][playedColumn].equals(" ");
    }

    /**
     * Check if there are three X or O in row.
     * @param row what row to check.
     * @return true if there are three of the same in that row, false otherwise.
     */
    private boolean checkRow(int row)
    {
        if(!board[row][0].equals(" "))
        {
            if(board[row][0].equals(board[row][1]) && board[row][0].equals(board[row][2]))
                return true;
        }
        return false;
    }

    /**
     * Check if there are three X or O in column.
     * @param col what row to check.
     * @return true if there are three of the same in that column, false otherwise.
     */
    private boolean checkCol(int col)
    {
        if(!board[0][col].equals(" "))
        {
            if(board[0][col].equals(board[1][col]) && board[0][col].equals(board[2][col]))
                return true;
        }
        return false;
    }

    /**
     * Check if there are three X or O diagonally across the board.
     * @return true if there are three of the same symbol diagonally across the board.
     */
    private boolean checkDiagonal()
    {
        if(!board[1][1].equals(" "))
        {
            if(board[0][0].equals(board[1][1]) && board[0][0].equals(board[2][2])
                    || board[0][2].equals(board[1][1]) && board[0][2].equals(board[2][0]))
                return true;
        }
        return false;
    }

    /**
     * Check entire board if we have a winner.
     */
    private void checkState()
    {
        if(checkRow(playedRow) || checkCol(playedColumn) || checkDiagonal())
        {
            System.out.println("We have a winner!");
            running = false;
        } else if (turns == 9) {
            System.out.println("IT'S A DRAW!");
            running = false;
        }
    }
}
