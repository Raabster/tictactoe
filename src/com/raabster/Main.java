package com.raabster;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Welcome to Tic Tac Toe!");
        System.out.println("Place your marker with the values 1-9.");
        boolean mainLoop = true;

	    while(mainLoop) {
            Game game = new Game();
            while(game.running)
            {
                game.playerAction();
                game.showBoard();
            }

            String input = "";
            Scanner scanner = new Scanner(System.in);
            boolean validInput = false;
            do {
                System.out.print("\nPlay again? Yes/No: ");
                input = scanner.nextLine();
                String regex_yes = "[Yy]\\w+|[Yy]";
                String regex_no = "[Nn]\\w+|[Nn]";
                if (input.matches(regex_yes)) {
                    System.out.println("\n\n\nAlright, here we go again!");
                    validInput = true;
                } else if (input.matches(regex_no)) {
                    System.out.println("Sad to see you go! Play again soon!");
                    validInput = true;
                    mainLoop = false;
                } else {
                    System.out.println("You must have done something wrong.");
                }
            } while (!validInput);
        }
    }
}
